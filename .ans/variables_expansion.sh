#!/usr/bin/env bash

#  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  
#  .--.              .--. 
# : (\ ". _......_ ." /) :  Owner: Shai
#  '.    `        `    .'   
#   /'   _        _   `\    Date: 21/09/2020
#  /     0}      {0     \   
# |       /      \       |  Version: 1.0.15
# |     /'        `\     |  
#  \   | .  .==.  . |   /   Purpose: Get user details
#   '._ \.' \__/ './ _./    print and prompt for change
#   /  ``'._-''-_.'``  \    finally save to JSON format
#           `--`
#  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

check_capital(){
######## get 1st. var as array name
local array_name="$1"
######## get all vars including 1st. as array property
local array=("$@")
######## remove 1st. var from duplicated array
unset array[0]
######## Re-index the keys
array=("${array[@]}")
i=0
len="${#array[@]}"
while [ $i -lt $len ];
	do
	ar="${array[$i]}"
######## Check for capital letters in duplicated array
		if [[ ${ar:0:1} != [0-9] && ${ar:0:1} == [a-z]  ]];then		
		 array[$i]="${ar^}"	
		else
			array[$i]="$ar"
		fi
	let i++
	done
######## use read to push to original array as we create dynamic array name
for ((i=0; i<${#array[@]}; ++i));
do
    read "$array_name[$i]" <<< "${array[i]}"
done
######## Finally destroy duplicated array so we can use it again if neccesary
unset array
}
create_arrays(){
answers=()
questions=("Name" "Last name" "ID")
len="${#questions[@]}"
count=0
while [ $count -lt $len ];
  do
########## read prompt 3 questions

	read -r -p "${questions[$count]} `echo $'\n> '` " line

########## Push to answers array

	answers+=("$line")
	let count++
  done

########## Check for Capital letter

check_capital "answers" "${answers[@]}"

########## Start Address Array - End prompt with empty line
  	addrs=()
while IFS= read -r -p "Address: (end with an empty line): `echo $'\n> '` " line; 
  do
    	[[ $line ]] || break
########## Push to address array

    	addrs+=("$line")
  done

########## Check for Capital letter 

check_capital "addrs" "${addrs[@]}"

########## Move to print the results function
print_arrays
}
update_arrays(){
# 0 = Name ; 1 = Last name ; 2 = ID ; 3 = Address
local idx=$1
	if [ "$idx" -ne "3" ];then # if 0-2 change personal array. if 3 recreate Address arr
	 read -p "${answers[$idx]} `echo $'\n> '`" line
	 answers[$idx]=$line
	 check_capital "answers" "${answers[@]}"
	else
	 addrs=()
	 while IFS= read -r -p "Address: (end with an empty line): `echo $'\n> '` " line; 
  	  do
    	   [[ $line ]] || break
    	   addrs+=("$line")
  	  done
	  check_capital "addrs" "${addrs[@]}"
	fi
print_arrays
}
print_arrays(){
printf '%s\n' "${answers[@]}"
printf '%s\n' "${addrs[@]}"
	read -p "Change anything? (y/n)`echo $'\n> '` " -n 1 -r
echo " "
	 if [[ $REPLY =~ ^[Yy]$ ]];then 
		case_change_arrs
	 else
		json_array
	fi
}
case_change_arrs(){
# ask the man if he wants to change anything
	select yna in "Name" "Last" "ID" "Address"; 
	 do
		case $yna in
			Name) update_arrays 0; break;;
			Last) update_arrays 1; break;;
			ID) update_arrays 2; break;;
			Address) update_arrays 3; break;;		
		esac
	 done
}
json_array() {
  local count=$((0-1))
  var='{'
	for line in "${answers[@]}"
	do
	let count++
	var+='"'${questions[$count]}'": "'$line'", '
	done
  var2=${var::-2}
  var2+=" "
  json_addr
}
json_addr(){
 local count=$((0-1))
 str_addrs=', "Location": {"Address": [{'
 var3="$var2 $str_addrs"
	for line in "${addrs[@]}"
	do
	let count++
	var3+='"a'$count'": "'$line'", '
	done
 var4=${var3::-2}
 var4+="}]}}"
 echo $var4 >> .credentials	
}

create_arrays
