#!/usr/bin/env bash

#  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  
#  .--.              .--. 
# : (\ ". _......_ ." /) :  Owner: Shai
#  '.    `        `    .'   
#   /'   _        _   `\    Date: 23/09/2020
#  /     0}      {0     \   
# |       /      \       |  Version: 1.0.1
# |     /'        `\     |  
#  \   | .  .==.  . |   /   Purpose: Factors
#   '._ \.' \__/ './ _./   
#   /  ``'._-''-_.'``  \   
#           `--`
#  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

calc(){
str=""
num=$1
arr=()
for (( i=1; i<$num; i++))
	do
		if [ $(( num % i )) == 0 ];then
			arr+=("$i")
		fi
	done
len=$((${#arr[@]}-1))
for (( i=$len; i>=0; i-- ))
	do
	if [[  "${arr[$i]}" -eq "7" ]];then
		str+="Plong"
	fi
	if [[  "${arr[$i]}" -eq "5" ]];then
                str+="Plang"
	fi
	if [[  "${arr[$i]}" -eq "3" ]];then
                str+="Pling"
	fi
	done
	if [ -z "$str" ];then
        	str="$num"
	fi
echo $str
}
calc 30
