#!/usr/bin/env bash

#    Owner: Shai
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Date: 26/09/2020
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Version: 1.0.0
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Purpose: use for loop in a farm
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

farm=("moose" "cow" "goose" "sow")
for i in "${farm[@]}"; do
	echo I have a $i
done
