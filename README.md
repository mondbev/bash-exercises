# Bash Exercises

## _Instructions_

This is the master module for Bash Exercises.
- Production code files will be stored here under [.ans](./.ans)
- For the staging code files you may request access to [exercises-test](https://gitlab.com/mondbev/bash-exercise-test) 
