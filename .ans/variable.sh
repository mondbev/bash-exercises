#!/usr/bin/env bash

#  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  
#  .--.              .--. 
# : (\ ". _......_ ." /) :  Owner: Shai
#  '.    `        `    .'   
#   /'   _        _   `\    Date: 21/09/2020
#  /     0}      {0     \   
# |       /      \       |  Version: 1.0.0
# |     /'        `\     |  
#  \   | .  .==.  . |   /   Purpose: Set user var
#   '._ \.' \__/ './ _./    ask for value - if empty
#   /  ``'._-''-_.'``  \    error, else greet
#           `--`
#  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

read -p "What's your name? `echo $'\n> '`" user_name
if [ ! -z $user_name ];then
	echo Hiya $user_name
else
	printf "%s\n" "Something went wrong" "exiting"
	exit 0
fi
