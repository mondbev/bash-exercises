#!/usr/bin/env bash

#    Owner: Shai
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Date: 26/09/2020
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Version: 1.0.1
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Purpose: Prompt user for town and street
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var=($@)
mytown="${var[0]}"
mystreet="${var[1]}"
	if [[ -z "$mytown" || -z "$mystreet" ]];then
		read -p "Town you grew up?  `echo $'\n> '`" mytown
		read -p "Street you grew up?  `echo $'\n> '`" mystreet
	fi
echo The street I grew up on was $mystreet and the town was $mytown

