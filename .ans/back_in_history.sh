#!/usr/bin/env bash

#    Owner: Shai
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Date: 26/09/2020
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Version: 1.0.0
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Purpose: use shift 
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ONE=$1
TWO=$2
THREE=$3
FOUR=$4
outp="There are $# paramaters that include $@. the first is $ONE, the second is $TWO,"
shift 3 
printf "%s\n" "$outp and the third is $@."
